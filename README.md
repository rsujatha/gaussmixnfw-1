## README ##

Code to implement Gaussian mixtures for halo (NFW) profiles using non-negative least squares fitting

### Requirements ###

* NumPy
* SciPy
* Jupyter (for example usage)

### Description ###

In progress

### Credit ###

* arXiv:20XX.YYYYY

### Contact ###

* Aseem Paranjape
