#!/opt/local/bin/python

import numpy as np
import scipy.special as sysp
import scipy.integrate as syint
from scipy import linalg
from scipy.optimize import nnls
import gc

################################################################
################################################################
class GaussMixNFW(object):
    """ Gaussian mixtures for NFW profile. """
    ##########################################################
    def __init__(self,profile='nfw',Rvir=1.0,cvir=10.0,rforce=2.0,non_neg=True,verbose=False):
        """ Implement Gaussian mixtures for NFW profile using (non-negative) least squares.
            Can be combined with concentration-mass relation fits.
             Initialise:
             -- profile  : string describing profile to fit. Currently only nfw supported.
             -- Rvir      : virial radius in Mpc/h
             -- cvir     : halo concentration
             -- rforce : force softening length in kpc/h
             -- non_neg: whether or not to use non-negative least squares (default True)
             -- verbose: control message printing
        """
        self.Rvir = Rvir
        self.cvir = cvir
        self.rforce = rforce
        self.verbose = verbose
        self.non_neg = non_neg
        self.profile = profile
        self.profile_list = ['nfw']
        if self.profile not in self.profile_list:
            raise ValueError("Profile must be one of: "+','.join([p for p in self.profile_list]))

        self.rs = self.Rvir/self.cvir # scale radius in Mpc/h
        self.f_cvir = self.func_f(self.cvir) # factor related to nfw normalisation

        # factor controlling number and spacing of Gaussian components
        # nn > 10 (i.e. NCOMP > 23) makes nnls fail
        self.nn = np.min([int(9*(self.Rvir/0.4)**0.107*(self.rforce/10.0)**0.05),10])

        # number of Gaussian components
        self.NCOMP = 2*self.nn+3
        if self.verbose:
            print"... using {0:d} Gaussian components".format(self.NCOMP)


        xfirst = 0.163*(self.rforce/5.0)**(-0.1)*(0.005/self.rs)*(self.Rvir/0.4)**0.7 
        # 0.163*(rforce/5.0)**(-0.1)*(0.005/rs)*(Rvir/0.4)**0.7
        xfirst = np.max([xfirst,0.2*(1e-3*self.rforce/self.rs)])
        xlast = 10*self.cvir
        xpivot = np.sqrt(xfirst*xlast)
        DY = np.log(xlast/xpivot)
        frac = 0.8

        #######################
        # Sampling points
        # -- anchored at xfirst, xlast
        # -- spreading outwards from 1 (r = r_s), pairwise symmetrically placed
        # -- NCOMP = 2*n + 3 
        #                    = (1) + (1+1) + (n) + (n)
        #                    = (x=1) + (xfirst + xlast) + (left of 1) + (right of 1)
        #######################
        self.XJ = np.ones(self.NCOMP,dtype=float) 
        self.XJ[0] = xfirst
        self.XJ[-1] = xlast
        if self.nn > 0:
            factor = np.exp(0.25*DY/self.nn)
            self.XJ[self.nn+2] = xpivot*factor
            self.XJ[self.nn] = xpivot/factor
            if self.nn > 1:
                for j in range(1,self.nn):
                    yj = (0.5*DY/self.nn)*(0.5 + j*(1 + frac*(self.nn-0.5)/(self.nn-1)))
                    self.XJ[self.nn+2+j] = xpivot*np.exp(yj)
                    self.XJ[self.nn-j] = xpivot/np.exp(yj)

        self.CJ = self.XJ*np.sqrt((1+self.XJ)/(1+3*self.XJ)) # gaussian widths 
        # dlngauss/dlnx = dlnnfw/dlnx at x=xmj if cj^2 = xmj^2(1+xmj)/(1+3xmj)

        xSones = np.ones(self.NCOMP-1,dtype=float) # sampling at Nc-1 points
        xCones = np.ones(self.NCOMP,dtype=float) # Nc components

        if self.profile=='nfw':
            func_profile  = self.nfw
        self.Nv = np.matrix(func_profile(self.XJ[1:])).T # NFW sampled at Nc-1 points

        Fmat = np.matrix(self.gauss(np.outer(self.XJ[1:],xCones),np.outer(xSones,self.CJ))) 
        # gauss(xSample | c, cComponent)
        FTF = Fmat.T*Fmat
        FTN = Fmat.T*self.Nv
        Mmat = np.zeros((self.NCOMP+1,self.NCOMP+1),dtype=float)
        Mmat[:self.NCOMP,:self.NCOMP] = FTF
        Mmat[:self.NCOMP,self.NCOMP] = 1.0
        Mmat[self.NCOMP,:self.NCOMP] = 1.0
        Mmat = np.matrix(Mmat)
        Sv = np.matrix(np.append(np.asarray(FTN.T),1)).T

        if self.non_neg:
            ######################
            # non-negative least squares subject to summation constraint
            ######################
            # minimise || M W - S ||^2 where everything is pos-semi-def
            # M is (Ns x Nc), W is (Nc x 1) and S is (Ns x 1) with Ns=Nc=NCOMP+1 in our case
            # Using https://en.wikipedia.org/wiki/Non-negative_least_squares 
            # implemented in scipy.optimize.nnls
            ######################
            self.WJ,rtol = nnls(np.asarray(Mmat),np.squeeze(np.asarray(Sv)))
        else:
            ######################
            # straightforward least squares, only subject to summation constraint
            ######################
            U,s,Vh = linalg.svd(Mmat)
            Minv = np.dot(Vh.T,np.dot(np.diag(1.0/s),U.T))
            self.WJ = Minv*Sv

        # Gaussian component weights
        self.WJ = np.squeeze(np.asarray(self.WJ[:-1]))
        
        # Gaussian component normalisations. Useful for external functions
        self.DJ = self.cvir**3/(3*self.func_g(self.cvir,self.CJ)) 

        # components to use in final mixture. can be redefined by user. 
        self.JVALS = np.arange(self.NCOMP)

        del xSones,xCones
        del Fmat,FTF,FTN,Mmat,Sv
        gc.collect()
    ##########################################################


    ##########################################################
    def func_f(self,x):
        """ Convenience function: int_0^x dy y / (1+y)^2 ."""
        out = np.log(1+x) - x/(1+x)
        return out
    ##########################################################


    ##########################################################
    def func_g(self,x,cj):
        """ Convenience function: int_0^x dy y^2 exp( - y^2 / (2 cj^2) ) ."""
        xBycjrt2 = x/cj/np.sqrt(2)
        out = np.sqrt(np.pi/2.0)*cj*sysp.erf(xBycjrt2) - x*np.exp(-xBycjrt2**2)
        out *= cj**2
        return out 
    ##########################################################

    ##########################################################
    def nfw(self,x):
        """ NFW profile normalised to unit enclosed overdensity inside Rvir."""
        out = self.cvir**3/(3*self.f_cvir*x*(1+x)**2)
        return out
    ##########################################################

    ##########################################################
    def gauss(self,x,cj):
        """ Gaussian profile normalised to unit enclosed overdensity inside Rvir. """
        out = self.cvir**3/(3*self.func_g(self.cvir,cj))*np.exp(-0.5*x**2/cj**2)
        return out
    ##########################################################

    ##########################################################
    def nfwGM(self,x):
        """ Gaussian mixture for NFW. """
        out = 0.0
        for j in iter(self.JVALS):
            out = out + self.WJ[j]*self.gauss(x,self.CJ[j])
        return out
    ##########################################################

    ##########################################################
    def gauss_avg(self,x,cj):
        """ Spherically averaged Gaussian profile normalised to unit enclosed overdensity inside Rvir. """
        out = (self.cvir/x)**3*self.func_g(x,cj)/self.func_g(self.cvir,cj)
        return out
    ##########################################################


    ##########################################################
    def nfwGM_avg(self,x):
        """ Spherically averaged Gaussian mixture for NFW. """
        out = 0.0
        for j in iter(self.JVALS):
            out = out + self.WJ[j]*self.gauss_avg(x,self.CJ[j])
        return out
    ##########################################################

################################################################



################################################################
class AnisoIntegrals(object):
    """ Integrals for use in anisotropic NFW modelling. """
    ##########################################################
    def __init__(self,verbose=True):
        """ Integrals for use in anisotropic NFW modelling."""
        self.nA = 4000
        self.MAX_FAC = 20
        self.nmu = 4000
        self.mu = np.linspace(-0.999999,0.999999,self.nmu,dtype='double')
        self.onemmu2 = 1.0 - self.mu**2
    ##########################################################


    ##########################################################
    def alpha_avg(self,Delta_avg,t_avg):
        """ Convenience function. 
            Returns alpha(<r) = (3/2)*|t(<r)|/Delta(<r)
        """
        out = 1.5*np.absolute(t_avg)/(Delta_avg + 1e-15)
        return out
    ##########################################################

    ##########################################################
    def int_Delta_avg_offcsat(self,r,dsat,B):
        """ int_0^{r/dsat*B} dA A exp(-A^2/(2B)) sinh(A) 
             Expect all arguments to be scalars.
             Units: r,rs,dsat (Mpc/h); B (dimless)
             Returns dimless scalar.
        """
        Amax = np.float64(r/dsat*B)
        Amin = np.float64(0.0)
        # func = lambda A,B : 0.5*A*np.exp(A*(1-0.5*A/B))*(1 - np.exp(-2*A))
        # out,err = syint.quad(func,Amin,Amax,args=(B,))
        # A = np.linspace(Amin,Amax,self.nA,dtype='double')
        # integrand = 0.5*A*np.exp(A*(1-0.5*A/B))*(1 - np.exp(-2*A))
        # integrand[~np.isfinite(integrand)] = 0.0
        # out = np.trapz(integrand,x=A)
        out = sysp.erf((r/dsat - 1.0)*np.sqrt(B/2)) + sysp.erf((r/dsat + 1.0)*np.sqrt(B/2))
        out *= np.sqrt(2*np.pi)*np.exp(0.5*B*(1+r/dsat)**2)
        out -= 2*(np.exp(2*Amax) - 1.0)/np.sqrt(B)
        out *= np.exp(-0.5*Amax*(2+r/dsat))*(B**1.5)/4
        return out        
    ##########################################################


    ##########################################################
    def Delta_avg_offcsat(self,r,rs,dsat,CJ,WJDJ):
        """ Spherically averaged overdensity Delta(<r) centered on 
             satellite at distance dsat from center of host with scale radius rs.
             r, dsat and rs should be scalars.
             CJ is array of widths c_j of Gaussian mixture components of host NFW profile.
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar Delta(<r).
        """
        Delta_sat_comp = np.zeros(CJ.size,dtype='double')
        for j in range(CJ.size):
            B = (dsat/(CJ[j]*rs))**2
            Delta_sat_comp[j] = (dsat/(r*B))**3*np.exp(-0.5*B)*self.int_Delta_avg_offcsat(r,dsat,B)
        cond = np.isfinite(Delta_sat_comp) 
        Delta_sat_comp[~cond] = 0.0
        cond = (Delta_sat_comp < 0.0)
        Delta_sat_comp[cond] = 0.0
        Delta_sat_comp *= WJDJ
        out = Delta_sat_comp.sum()
        return out
    ##########################################################
    

    ##########################################################
    def int_t_avg_offcsat(self,r,dsat,B):
        """ int_{r/dsat*B}^infty dA/A^4 exp(-A^2/(2B)) [(A^2 + 3)*sinh(A) - 3*A*cosh(A)] 
             Expect all arguments to be scalars.
             Units: r,rs,dsat (Mpc/h); sigj (dimless)
             Returns dimless scalar.
        """
        # need Amax >> 2B for convergence
        Amin = r/dsat*B
        cond1 = (Amin > 3.0) & (B < 0.1*Amin)
        cond2 = (Amin < 1.0) & (B < 0.1*Amin**2)
        if (cond1 | cond2):
            x = B/Amin**2
            fmin = ((Amin**2+3)*np.sinh(Amin) - 3*A*np.cosh(Amin))/Amin**4
            g1 = (Amin*np.cosh(Amin) - np.sinh(Amin))/(Amin**2*fmin)
            g2 = g1 - 0.1*np.sinh(Amin)/fmin
            out = 1.0 + x*(g1 - 5.0) + x**2*(35.0 - 10*g2)
            out *= (B/Amin)*fmin*np.exp(-0.5/x)
        else:
            Amax = self.MAX_FAC*np.max([2.0*B,Amin])
            # func = lambda A,B : np.exp(-0.5*A**2/B)/A**4*((A**2+3)*np.sinh(A) - 3*A*np.cosh(A))
            # out,err = syint.quad(func,Amin,Amax,args=(B,))
            A = np.logspace(np.log10(Amin),np.log10(Amax),self.nA,dtype='double')
            dlnA = np.log(A[1]/A[0])
            integrand = (np.exp(A*(1 - 0.5*A/B))/(2*A**3)*((A**2+3)*(1-np.exp(-2*A)) 
                                                           - 3*A*(1+np.exp(-2*A))))
            integrand[~np.isfinite(integrand)] = 0.0
            out = np.trapz(integrand,dx=dlnA)
        return out        
    ##########################################################


    ##########################################################
    def t_avg_offcsat(self,r,rs,dsat,CJ,WJDJ):
        """ Spherically averaged tidal scalar t(<r) centered on 
             satellite at distance dsat from center of host with scale radius rs.
             r, dsat and rs should be scalars.
             CJ is array of widths c_j of Gaussian mixture components of host NFW profile.
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar t(<r).
        """
        t_sat_comp = np.zeros(CJ.size,dtype='double')
        for j in range(CJ.size):
            B = (dsat/(CJ[j]*rs))**2
            t_sat_comp[j] = 2*np.exp(-0.5*B)*self.int_t_avg_offcsat(r,dsat,B)
        cond = np.isfinite(t_sat_comp)
        t_sat_comp[~cond] = 0.0
        t_sat_comp *= WJDJ
        out = t_sat_comp.sum()
        return out
    ##########################################################


    ##########################################################
    def Delta_avg_filaxis(self,r,RJ,WJDJ):
        """ Spherically averaged overdensity Delta(<r) [r is scalar in Mpc/h] centered on 
             host at axis of filament described by Gaussian mixture of radii RJ [in Mpc/h]
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar Delta(<r).
        """
        A = np.zeros(RJ.size,dtype='double')
        A[:] = r/(np.sqrt(2)*RJ).astype('double')
        Delta_fil_comp = 1.5/A**2*(1 - sysp.dawsn(A)/A)

        cond = np.isfinite(Delta_fil_comp) 
        Delta_fil_comp[~cond] = 0.0
        cond = (Delta_fil_comp < 0.0)
        Delta_fil_comp[cond] = 0.0
        Delta_fil_comp *= WJDJ
        out = Delta_fil_comp.sum()
        return out
    ##########################################################


    ##########################################################
    def t_avg_filaxis(self,r,RJ,WJDJ):
        """ Spherically averaged tidal scalar t(<r) [r is scalar in Mpc/h] centered on 
             host at axis of filament described by Gaussian mixture of radii RJ [in Mpc/h].
             t(<R) = (1/3)<Delta>(<R) for any Gaussian mixture filament.
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar t(<r).
        """
        return self.Delta_avg_filaxis(r,RJ,WJDJ)/3.0
    ##########################################################


    ##########################################################
    def Delta_avg_anisohalo(self,r,rs,b,CJ,WJDJ):
        """ Spherically averaged overdensity Delta(<r) centered on 
             anisotropic NFW halo with scale radius rs and axis ratio b.
             r, b and rs should be scalars.
             CJ is array of widths c_j of Gaussian mixture components of halo NFW profile.
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar Delta(<r).
        """
        Delta_ahalo_comp = np.zeros(CJ.size,dtype='double')
        A = r/(CJ*rs)
        kappa = A*np.sqrt((1-b**2)/2.0)/b
        Delta_ahalo_comp = np.sqrt(np.pi/2.0)*sysp.erf(A/np.sqrt(2.0))
        Delta_ahalo_comp -= b*np.sqrt(2/(1-b**2))*np.exp(-0.5*A**2)*sysp.dawsn(kappa)
        Delta_ahalo_comp *= 3*b**2/A**3 
        cond = np.isfinite(Delta_ahalo_comp) 
        Delta_ahalo_comp[~cond] = 0.0
        cond = (Delta_ahalo_comp < 0.0)
        Delta_ahalo_comp[cond] = 0.0
        Delta_ahalo_comp *= WJDJ
        out = Delta_ahalo_comp.sum()
        return out
    ##########################################################

    ##########################################################
    def int_t_avg_anisohalo(self,kappa,b):
        """ (3/2)*int_{kappa}^infty ds/s^2 exp(-s^2b^2/(1-b^2)) [(1/s) - (2/3 + 1/s^2)D(s)] 
             Expect all arguments to be scalars.
             Units: r,rs,dsat (Mpc/h); sigj (dimless)
             Returns dimless scalar.
        """
        smax = self.MAX_FAC*kappa
        s = np.logspace(np.log10(kappa),np.log10(smax),self.nA,dtype='double')
        dlns = np.log(s[1]/s[0])
        integrand = (1.0/s)*np.exp(-(b*s)**2/(1-b**2))*(1.0/s - (2/3.0+1/s**2)*sysp.dawsn(s))
        integrand[~np.isfinite(integrand)] = 0.0
        out = np.trapz(integrand,dx=dlns)
        return out        
    ##########################################################


    ##########################################################
    def t_avg_anisohalo(self,r,rs,b,CJ,WJDJ):
        """ Spherically averaged tidal scalar t(<r) centered on 
             anisotropic NFW halo with scale radius rs and axis ratio b.
             r, b and rs should be scalars.
             CJ is array of widths c_j of Gaussian mixture components of halo NFW profile.
             WJDJ is array of products of weights w_j and normalisations Delta_j of component Gaussians.
             Returns scalar t(<r).
        """
        t_ahalo_comp = np.zeros(CJ.size,dtype='double')
        for j in range(CJ.size):
            kappa = r/(CJ[j]*rs)*np.sqrt(0.5*(1-b**2))/b
            t_ahalo_comp[j] = self.int_t_avg_anisohalo(kappa,b)
        cond = np.isfinite(t_ahalo_comp) 
        t_ahalo_comp[~cond] = 0.0
        cond = (t_ahalo_comp < 0.0)
        t_ahalo_comp[cond] = 0.0
        t_ahalo_comp *= WJDJ
        out = t_ahalo_comp.sum()
        return out
    ##########################################################


################################################################

if __name__=="__main__":

    Rvir = 1.0 
    cvir = 10.0 
    rforce = 2.5
    xmin = 0.0025*cvir
    xmax = 15*cvir
    nx = 20
    xv = np.logspace(np.log10(xmin),np.log10(xmax),nx)

    gm = GaussMixNFW(Rvir=Rvir,cvir=cvir)

    print 'Rvir,cvir,rs,rforce: ',gm.Rvir,gm.cvir,gm.rs,gm.rforce
    print gm.nfwGM(xv)
